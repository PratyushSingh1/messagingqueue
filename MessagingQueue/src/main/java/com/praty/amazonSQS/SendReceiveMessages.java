package com.praty.amazonSQS;

import java.util.Date;
import java.util.List;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.AmazonSQSException;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class SendReceiveMessages {

	 private static final String QUEUE_NAME = "testQueue" + new Date().getTime();

	    public static void main(String[] args) throws Exception
	    {
	    	BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIWHNNQMDVUBUJHTQ", 
	    			"VU9Sq4l0rU+gvsII46+xNhLcKQh167ij4kbKKIqd");
	    	
	        final AmazonSQS sqs = AmazonSQSClientBuilder
	        		.standard()
	        		.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
	        		.withRegion("us-east-2").build();

	        try {
	            CreateQueueResult create_result = sqs.createQueue(QUEUE_NAME);
	        } catch (AmazonSQSException e) {
	            if (!e.getErrorCode().equals("QueueAlreadyExists")) {
	                throw e;
	            }
	        }

	        String queueUrl = sqs.getQueueUrl(QUEUE_NAME).getQueueUrl();

	        SendMessageRequest send_msg_request = new SendMessageRequest()
	                .withQueueUrl(queueUrl)
	                .withMessageBody("hello world")
	                .withDelaySeconds(5);
	        sqs.sendMessage(send_msg_request);


	        // Send multiple messages to the queue
	        SendMessageBatchRequest send_batch_request = new SendMessageBatchRequest()
	                .withQueueUrl(queueUrl)
	                .withEntries(
	                        new SendMessageBatchRequestEntry(
	                                "msg_1", "Hello from message 1"),
	                        new SendMessageBatchRequestEntry(
	                                "msg_2", "Hello from message 2")
	                                .withDelaySeconds(10));
	        sqs.sendMessageBatch(send_batch_request);

	        //Thread.sleep(10000);
	        
	        // receive messages from the queue
	        ReceiveMessageRequest receive_request = new ReceiveMessageRequest()
	                .withQueueUrl(queueUrl)
	                .withWaitTimeSeconds(20);
	        List<Message> messages = sqs.receiveMessage(receive_request).getMessages();

	        // delete messages from the queue
	        for (Message m : messages) {
	        	System.out.println("messages are-->"+m.getBody());
	            //sqs.deleteMessage(queueUrl, m.getReceiptHandle());
	        }
	    }
}
